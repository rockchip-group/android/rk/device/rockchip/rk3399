#!/system/bin/sh

#############################################
#
# Version Information
#
#   v1.0 first version, only test on rk3399 linux kernel 4.4
#
#		Authors: zwp@rock-chips.com
#
#############################################

#############################################
#       monitor hardware information
# the information format:
#	UPTIME(s)       CPU(LF/BF/L/T)  GPU(F/L/T)      VPU/HEVC(F)     DDR(F/L)        FPS
#	5       1200/1416/01/41 200/00/40       000/000         400/09          0.0
#	5       1200/1416/03/41 200/00/40       000/000         400/69          0.0
#	The meaning of charactors:
# LF: the frequence of cpu_little; BF: the freqence of cpu_big
# F: frequence; L: load; T: temperature
#
#############################################

function print_system_info(){
	echo "######################################################################"
	echo ""
	echo "[Model]: " `getprop ro.product.model`
	echo "[Firmware]: " `getprop ro.build.description`
	echo "[Kernel]: " `cat /proc/version`
	echo "LF: the frequence of cpu_little; BF: the freqence of cpu_big; F: frequence; L: load; T: temperature"
	echo ""
	echo "######################################################################"

	if [ $cpu_cluster_num -eq 2 ]; then
		echo "UPTIME(s)\tCPU(LF/BF/L/T)\tGPU(F/L/T)\tVPU/HEVC(F)\tDDR(F/L)\tFPS"
	else
		echo "UPTIME(s)\tCPU(F/L/T)\tGPU(F/L/T)\tVPU/HEVC(F)\tDDR(F/L)\tFPS"
	fi

	# get chip id
	chip_type=`getprop ro.product.model`
	chip_type=`expr ${chip_type: 0: 6}` # the value will be "rkxxxx"
	echo "this chip is " $chip_type
}

function get_system_info(){
	#the value of android_cpu_abi is armeabi-v7a or arm64-v8a
	android_cpu_abi=`getprop ro.product.cpu.abi`
	linux_ver=`cat /proc/version`
	linux_ver_major=`expr ${linux_ver: 14: 1}`
	linux_ver_minor=`expr ${linux_ver: 16: 1}`
	if [ ${linux_ver_major} -lt 4 ]; then
		echo "Only support linux version greater than 4.4"
		exit 1
	fi
	up_time=`uptime | busybox awk '{print substr($1,0,8)}'`
}

prev_cpu_use=0
prev_cpu_total=0
function get_cpu_info(){
	# whether cpu has 2 cluster
	if [ -d "/sys/devices/system/cpu/cpufreq/policy4/" ] ; then
		cpu_cluster_num=2
	fi
	
	#get cpu frequence
	if [ $cpu_cluster_num -eq 2 ] ; then
		cpuB_freq=`cat /sys/devices/system/cpu/cpufreq/policy4/scaling_cur_freq`
		cpuB_freq=$(($cpuB_freq/1000))
	else
		cpuB_freq=""
	fi
	cpuL_freq=`cat /sys/devices/system/cpu/cpufreq/policy0/scaling_cur_freq`
	cpuL_freq=$(($cpuL_freq/1000))

	#get cpu loading
	cpu_load=0
	eval $(cat /proc/stat | grep "cpu " | busybox awk '
	{
	    printf("cpu_use=%d; cpu_total=%d;",$2+$3+$4+$6+$7+$8, $2+$3+$4+$5+$6+$7+$8)
	}
	')
	cpu_load=$((($cpu_use-$prev_cpu_use)*100/($cpu_total-$prev_cpu_total)))
	prev_cpu_use=$cpu_use
	prev_cpu_total=$cpu_total


	#get cpu thermal temperature
	cpu_temp=0
	if [ -f "/sys/class/thermal/thermal_zone0/temp" ]; then # rk3399
		cpu_temp=`cat /sys/class/thermal/thermal_zone0/temp`
		cpu_temp=$(($cpu_temp/1000))
	elif [ -f "/sys/devices/11150000.tsadc/temp0_input" ]; then # rk3228
		cpu_temp=`cat /sys/devices/11150000.tsadc/temp0_input`
	fi
}

function get_gpu_info(){
	gpu_freq=0
	gpu_load=0

	# get gpu freq and loading, /* TODO, current only support rk3399, add other chips */
	if [ -f "/sys/class/devfreq/ff9a0000.gpu/available_frequencies" ]; then
		gpu_freq=`cat /sys/class/devfreq/ff9a0000.gpu/cur_freq`
		gpu_freq=$(($gpu_freq/1000000))

		eval $(cat /sys/class/devfreq/ff9a0000.gpu/load | busybox awk  -F '@' '
		{
		    printf("gpu_load=%d",$1)
		}
		')
	fi
	
	# get gpu temperature
	gpu_temp=0
	if [ -f "/sys/class/thermal/thermal_zone1/temp" ]; then
		gpu_temp=`cat /sys/class/thermal/thermal_zone1/temp`
		gpu_temp=$(($gpu_temp/1000))
	fi
}

function get_ddr_info(){
	if [ -d "/sys/class/devfreq/dmc" ]; then
		ddr_freq=`cat /sys/class/devfreq/dmc/cur_freq`
		ddr_freq=$(($ddr_freq/1000000))
		
		eval $(cat /sys/class/devfreq/dmc/load | busybox awk  -F '@' '
		{
		    printf("ddr_load=%d",$1)
		}
		')
	else
		#if dmc node in dts is not enabled, we have to get ddr frequence from clk_summary
		if [ $chip_type == "rk3399" ]; then
			eval $(cat /d/clk/clk_summary | grep " sclk_ddrc" | busybox awk '
			{
				printf("ddr_freq=%d;",$4);
			}
			')
			ddr_freq=$(($ddr_freq/1000000))
			# because we can't get ddr frequence if dmc is not enabled, so we set ddr_load to -1.
			ddr_load=-1
		else
			ddr_freq=-1
			ddr_load=-1
		fi
	fi
}

function get_vpu_info(){
	vdpu_freq=0
	eval $(cat /d/clk/clk_summary | grep " clk_vdpu" | busybox awk '
	{
	    if($2<=0)
	        printf("vdpu_freq=%d;", 0);
	    else
	        printf("vdpu_freq=%d;", $4/1000000);
	}
	')
	if [ $vdpu_freq == 0 ]; then
		eval $(cat /d/clk/clk_summary | grep "aclk_vdu" | busybox awk '
		{
		    if($2<=0)
		        printf("vdpu_freq=%d;", 0);
		    else
		        printf("vdpu_freq=%d;", $4/1000000);
		}
		')
	fi
	
	hevc_freq=0
	eval $(cat /d/clk/clk_summary | grep "clk_vdu_core" | busybox awk '
	{
	    if($2<=0)
	        printf("hevc_freq=%d;", 0);
	    else
	        printf("hevc_freq=%d;", $4/1000000);
	}
	')
}

function get_fps_info(){
	app_fps=0
	#we only record fps from last second to now
	cur_time_sec=`date +%s`
	cur_time_sec=`expr $cur_time_sec - 1` # the time of last second
	log_start_time=`date '+%m-%d %H:%M:%S.000' -d @$cur_time_sec`


	setprop debug.sf.fps 1
	setprop debug.hwc.logfps 1

	eval $(logcat -v time -t "$log_start_time" | grep -E "SurfaceFlinger.*mFps" | busybox awk -F"mFps = " '
	{
		printf("app_fps=%f;", $2);
	}
	')
}

function do_exit(){
	setprop debug.sf.fps 0
	setprop debug.hwc.logfps 0
	exit 1
}

function get_info(){
	get_system_info
	get_cpu_info
	get_gpu_info
	get_ddr_info
	get_vpu_info
	get_fps_info
}

trap 'do_exit' INT TSTP

get_info
print_system_info

while true
do
	get_info
	if [ $cpu_cluster_num -eq 2 ]; then
		busybox printf "%s\t%03d/%02d/%02d/%02d\t%03d/%02d/%02d\t%03d/%03d\t\t%03d/%02d\t\t%.1f\n" $up_time $cpuL_freq $cpuB_freq $cpu_load $cpu_temp $gpu_freq $gpu_load $gpu_temp $vdpu_freq $hevc_freq $ddr_freq $ddr_load $app_fps
	else
		busybox printf "%s\t%03d/%02d/%02d\t%03d/%02d/%02d\t%03d/%03d\t\t%03d/%02d\t\t%.1f\n" $up_time $cpuL_freq $cpu_load $cpu_temp $gpu_freq $gpu_load $gpu_temp $vdpu_freq $hevc_freq $ddr_freq $ddr_load $app_fps
	fi
	sleep 1
done

do_exit