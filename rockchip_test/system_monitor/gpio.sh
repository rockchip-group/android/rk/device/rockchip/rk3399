#!/system/bin/sh

#############################################
#
# Version Information
#
#   v1.0 first version
#
#		Authors: zwp@rock-chips.com
#
#############################################

#############################################
#       gpio iomux configs set and read
#
#
#############################################

# The format of funcs_table:
# every row is define for one gpio, the meaning of columns is as below:
# column 1: gpio name
# column 2: the iomux register address
# column 3: the offset for this gpio in the iomux register, if it's "-1" means this gpio don't have iomux configs.
# column 4: the mask value for this gpio in the iomux register, if it's "rsv" means this gpio don't have iomux configs.
# column 5: function 0
# column 6: function 1
# column 7: function 2
# column 8: function 3
# column 9: function 4, not every chip has function 4

funcs_table_px30=(
gpio0a0	0xff010000 0  0x3    gpio clk_out_wifi	0	0	0
gpio0a1	0xff010000 2  0xc    gpio 0	0	0	0
gpio0a2	0xff010000 4  0x30   gpio 0	0	0	0
gpio0a3	0xff010000 6  0xc0   gpio sdmmc0_detn	0	0	0
gpio0a4	0xff010000 8  0x300  gpio pmic_sleep	tsadc_shutm1	0	0
gpio0a5	0xff010000 10 0xc00  gpio 0	0	0	0
gpio0a6	0xff010000 12 0x3000 gpio tsadc_shutm0	tsadc_shutorg	0	0
gpio0a7	0xff010000 14 0xc000 gpio 0	0	0	0
gpio0b0	0xff010004 0  0x3    gpio i2c0_scl	0	0	0
gpio0b1	0xff010004 2  0xc    gpio i2c0_sda	0	0	0
gpio0b2	0xff010004 4  0x30   gpio uart0_tx	0	0	0
gpio0b3	0xff010004 6  0xc0   gpio uart0_rx	0	0	0
gpio0b4	0xff010004 8  0x300  gpio uart0_cts	0	0	0
gpio0b5	0xff010004 10 0xc00  gpio uart0_rts	test_clk1	0	0     
gpio0b6	0xff010004 12 0x3000 gpio flash_vol_sel	0	0	0
gpio0b7	0xff010004 14 0xc000 gpio pwm_0	otg_drv	0	0
gpio0c0	0xff010008 0  0x3    gpio pwm_1	uart3_txm0	0	0
gpio0c1	0xff010008 2  0xc    gpio pwm_3	uart3_rxm0	0	0
gpio0c2	0xff010008 4  0x30   gpio i2c1_scl	uart3_ctsm0	0	0
gpio0c3	0xff010008 6  0xc0   gpio i2c1_sda	uart3_rtsm0	0	0
gpio0c4	0xff010008 8  0x300  gpio clk_inout_32k	0	0	0
gpio0c5	0xff010008 10 0xc00  gpio 0 0 0 0
gpio0c6	0xff010008 12 0x3000 gpio 0 0 0 0
gpio0c7	0xff010008 14 0xc000 gpio 0 0 0 0
gpio1a0	0xff140000 0  0xf     gpio flash_d0	emmc_d0	sfc_sio0	0
gpio1a1	0xff140000 4  0xf0    gpio flash_d1	emmc_d1	sfc_sio1	0
gpio1a2	0xff140000 8  0xf00   gpio flash_d2	emmc_d2	sfc_sio2	0
gpio1a3	0xff140000 12  0xf000   gpio flash_d3	emmc_d3	sfc_sio3	0
gpio1a4	0xff140004 0  0xf     gpio flash_d4	emmc_d4	sfc_csn0	0
gpio1a5	0xff140004 4  0xf0    gpio flash_d5	emmc_d5	0	0
gpio1a6	0xff140004 8  0xf00   gpio flash_d6	emmc_d6	0	0
gpio1a7	0xff140004 12  0xf000 gpio flash_d7	emmc_d7	0	0
gpio1b0	0xff140008 0  0xf     gpio flash_cs0	emmc_pwren	0	0
gpio1b1	0xff140008 4  0xf0    gpio flash_rdy	emmc_clkout	sfc_clk	0
gpio1b2	0xff140008 8  0xf00   gpio flash_dqs	emmc_cmd	0	0
gpio1b3	0xff140008 12  0xf000 gpio flash_ale	emmc_rstn	0	0
gpio1b4	0xff14000c 0  0xf     gpio flash_cle	uart3_ctsm1	spi0_mosi	i2c3_sda
gpio1b5	0xff14000c 4  0xf0    gpio flash_wrn	uart3_rtsm1	spi0_miso	i2c3_scl
gpio1b6	0xff14000c 8  0xf00   gpio flash_cs1	uart3_txm1	spi0_csn	0
gpio1b7	0xff14000c 12  0xf000 gpio flash_rdn	uart3_rxm1	spi0_clk	0
gpio1c0	0xff140010 0  0xf     gpio uart1_rx	0	0	0
gpio1c1	0xff140010 4  0xf0    gpio uart1_tx	0	0	0
gpio1c2	0xff140010 8  0xf00   gpio uart1_cts	0	0	0
gpio1c3	0xff140010 12  0xf000 gpio uart1_rts	0	0	0
gpio1c4	0xff140014 0  0xf     gpio sdmmc1_cmd	0	0	0
gpio1c5	0xff140014 4  0xf0    gpio sdmmc1_clk	0	0	0
gpio1c6	0xff140014 8  0xf00   gpio sdmmc1_d0	0	0	0
gpio1c7	0xff140014 12  0xf000 gpio sdmmc1_d1	0	0	0
gpio1d0	0xff140018 0  0xf     gpio sdmmc1_d2	0	0	0
gpio1d1	0xff140018 4  0xf0    gpio sdmmc1_d3	0	0	0
gpio1d2	0xff140018 8  0xf00   gpio sdmmc0_d0	uart2_txm0	0	0
gpio1d3	0xff140018 12  0xf000 gpio sdmmc0_d1	uart2_rxm0	0	0
gpio1d4	0xff14001c 0  0xf     gpio sdmmc0_d2	uart4_rx	jtag_tck	0
gpio1d5	0xff14001c 4  0xf0    gpio sdmmc0_d3	uart4_tx	jtag_tms	0
gpio1d6	0xff14001c 8  0xf00   gpio sdmmc0_clko	uart4_cts	test_clko	0
gpio1d7	0xff14001c 12  0xf000 gpio sdmmc0_cmd	uart4_rts	0	0
gpio2a0	0xff140020 0  0xf     gpio cif_d2m0	rmii_txen	0	0
gpio2a1	0xff140020 4  0xf0    gpio cif_d3m0	rmii_txd1	0	0
gpio2a2	0xff140020 8  0xf00   gpio cif_d4m0	rmii_txd0	0	0
gpio2a3	0xff140020 12  0xf000 gpio cif_d5m0	rmii_rxd0	0	0
gpio2a4	0xff140024 0  0xf     gpio cif_d6m0	rmii_rxd1	0	0
gpio2a5	0xff140024 4  0xf0    gpio cif_d7m0	rmii_rxer	0	0
gpio2a6	0xff140024 8  0xf00   gpio cif_d8m0	rmii_rxdv	0	0
gpio2a7	0xff140024 12  0xf000 gpio cif_d9m0	rmii_mdio	0	0
gpio2b0	0xff140028 0  0xf     gpio cif_vsyncm0	0	0	0
gpio2b1	0xff140028 4  0xf0    gpio cif_hrefm0	rmii_mdc	0	0
gpio2b2	0xff140028 8  0xf00   gpio cif_clkim0	rmii_clk	0	0
gpio2b3	0xff140028 12  0xf000 gpio cif_clkom0	clk_out_ethernet	0	0
gpio2b4	0xff14002c 0  0xf     gpio cif_d0m0	uart2_txm1	0	0
gpio2b5	0xff14002c 4  0xf0    gpio pwm_2	0	0	0
gpio2b6	0xff14002c 8  0xf00   gpio cif_d1m0	uart2_rxm1	0	0
gpio2b7	0xff14002c 12  0xf000 gpio cif_d10m0	i2c2_scl	0	0
gpio2c0	0xff140030 0  0xf     gpio cif_d11m0	i2c2_sda	0	0
gpio2c1	0xff140030 4  0xf0    gpio i2s1_2ch_lrck	0	0	0
gpio2c2	0xff140030 8  0xf00   gpio i2s1_2ch_sclk	0	0	0
gpio2c3	0xff140030 12  0xf000 gpio i2s1_2ch_mclk	0	0	0
gpio2c4	0xff140034 0  0xf     gpio i2s1_2ch_sdo	0	0	0
gpio2c5	0xff140034 4  0xf0    gpio i2s1_2ch_sdi	pdm_sdi0m1	0	0
gpio2c6	0xff140034 8  0xf00   gpio pdm_clk0m1	0	0	0      
gpio2c7	0xff140034 12  0xf000   gpio 0	0	0	0
gpio3a0	0xff140040 0  0xf     gpio lcdc_clk	0	0	0
gpio3a1	0xff140040 4  0xf0    gpio lcdc_hsyncm0	i2s2_2ch_mclk	cif_d0m1	uart5_rx
gpio3a2	0xff140040 8  0xf00   gpio lcdc_vsyncm0	i2s2_2ch_sclk	cif_d1m1	uart5_tx
gpio3a3	0xff140040 12  0xf000 gpio lcdc_denm0	i2s2_2ch_lrck	cif_d2m1	uart5_cts
gpio3a4	0xff140044 0  0xf     gpio lcdc_d0	0	0	0
gpio3a5	0xff140044 4  0xf0    gpio lcdc_d1m0	i2s2_2ch_sdi	cif_d3m1	uart5_rts
gpio3a6	0xff140044 8  0xf00   gpio lcdc_d2	0	0	0
gpio3a7	0xff140044 12  0xf000 gpio lcdc_d3m0	i2s2_2ch_sdo	cif_d4m1	0
gpio3b0	0xff140048 0  0xf     gpio lcdc_d4m0	i2s0_8ch_sdi3	cif_d5m1	0
gpio3b1	0xff140048 4  0xf0    gpio lcdc_d5m0	i2s0_8ch_sdi2	cif_d6m1	spi1_csn
gpio3b2	0xff140048 8  0xf00   gpio lcdc_d6	spi1_csn1	0	0
gpio3b3	0xff140048 12  0xf000 gpio lcdc_d7	i2s0_8ch_sdi1	0	0
gpio3b4	0xff14004c 0  0xf     gpio lcdc_d8m0	i2s0_8ch_sclkrx	cif_d7m1	spi1_mosi
gpio3b5	0xff14004c 4  0xf0    gpio lcdc_d9m0	i2s0_8ch_lrckrx	0	0
gpio3b6	0xff14004c 8  0xf00   gpio lcdc_d10m0	i2s0_8ch_sdo3	cif_d8m1	spi1_miso
gpio3b7	0xff14004c 12  0xf000 gpio lcdc_d11m0	i2s0_8ch_sdo2	cif_d9m1	spi1_clk
gpio3c0	0xff140050 0  0xf     gpio lcdc_d12	i2s0_8ch_sdo1	0	0
gpio3c1	0xff140050 4  0xf0    gpio lcdc_d13	i2s0_8ch_mclk	0	0
gpio3c2	0xff140050 8  0xf00   gpio lcdc_d14	i2s0_8ch_lrcktx	pwm_4	tdm_fsync
gpio3c3	0xff140050 12  0xf000 gpio lcdc_d15	i2s0_8ch_sclktx	pwm_5	tdm_sclk
gpio3c4	0xff140054 0  0xf     gpio lcdc_d16	i2s0_8ch_sdo0	pwm_6	tdm_sdo
gpio3c5	0xff140054 4  0xf0    gpio lcdc_d17	i2s0_8ch_sdi0	pwm_7	tdm_sdi
gpio3c6	0xff140054 8  0xf00   gpio lcdc_d18	pdm_clk0m0	cif_d10m1	0
gpio3c7	0xff140054 12  0xf000 gpio lcdc_d19	pdm_clk1	cif_d11m1	0
gpio3d0	0xff140058 0  0xf     gpio lcdc_d20	pdm_sdi1	cif_clkoutm1	0
gpio3d1	0xff140058 4  0xf0    gpio lcdc_d21	pdm_sdi2	cif_vsyncm1	isp_prelight_trig 
gpio3d2	0xff140058 8  0xf00   gpio lcdc_d22	pdm_sdi3	cif_hrefm1	isp_flash_trigout
gpio3d3	0xff140058 12  0xf000 gpio lcdc_d23	pdm_sdi0m0	cif_clkinm1	isp_flash_trigin
gpio3d4 0xff14005c 0  0xf     gpio 0 0 0 0
gpio3d5 0xff14005c 4  0xf0    gpio 0 0 0 0
gpio3d6 0xff14005c 8  0xf00   gpio 0 0 0 0
gpio3d7 0xff14005c 12  0xf000 gpio 0 0 0 0
);

funcs_table_rk3368=(
gpio0a0 0xff738000 0  0x3    gpio	global_pwroff	pmic_sleep	0
gpio0a1 0xff738000 2  0xc    gpio	0	0	0
gpio0a2 0xff738000 4  0x30   gpio	0	0	0
gpio0a3 0xff738000 6  0xc0   gpio	tsadc_int	pmu_debug0	0
gpio0a4 0xff738000 8  0x300  gpio	0	0	0
gpio0a5 0xff738000 10 0xc00  gpio	0	0	0
gpio0a6 0xff738000 12 0x3000 gpio	i2c0pmu_sda	0	0
gpio0a7 0xff738000 14 0xc000 gpio	i2c0pmu_scl	0	0
gpio0b0 0xff738004 0  0x3    gpio	test_clkout	pwm_1	pmu_debug1
gpio0b1 0xff738004 2  0xc    gpio	sc_vcc33v	i2c2sensor_scl	gpujtag_trstn
gpio0b2 0xff738004 4  0x30   gpio	sc_rst	spi2_rxd	gpujtag_tms
gpio0b3 0xff738004 6  0xc0   gpio	sc_clk	spi2_txd	gpujtag_tdi
gpio0b4 0xff738004 8  0x300  gpio	sc_io	spi2_clk	gpujtag_tdo
gpio0b5 0xff738004 10 0xc00  gpio	sc_detect	spi2_csn0	0
gpio0b6 0xff738004 12 0x3000 gpio	lcdc_data10	trace_data0	jtag_trstn
gpio0b7 0xff738004 14 0xc000 gpio	lcdc_data11	trace_data1	jtag_tdi
gpio0c0 0xff738008 0  0x3    gpio	lcdc_data12	trace_data2	jtag_tdo
gpio0c1 0xff738008 2  0xc    gpio	lcdc_data13	trace_data3	mcujtag_trstn
gpio0c2 0xff738008 4  0x30   gpio	lcdc_data14	trace_data4	mcujtag_tdi
gpio0c3 0xff738008 6  0xc0   gpio	lcdc_data15	trace_data5	mcujtag_tdo
gpio0c4 0xff738008 8  0x300  gpio	lcdc_data16	trace_data6	uart1bb_sin
gpio0c5 0xff738008 10 0xc00  gpio	lcdc_data17	trace_data7	uart1bb_sout
gpio0c6 0xff738008 12 0x3000 gpio	lcdc_data18	trace_data8	uart1bb_ctsn
gpio0c7 0xff738008 14 0xc000 gpio	lcdc_data19	trace_data9	uart1bb_rtsn
gpio0d0 0xff73800c 0  0x3    gpio	lcdc_data20	trace_data10	uart4exp_ctsn
gpio0d1 0xff73800c 2  0xc    gpio	lcdc_data21	trace_data11	uart4exp_rtsn
gpio0d2 0xff73800c 4  0x30   gpio	lcdc_data22	trace_data12	uart4exp_sout
gpio0d3 0xff73800c 6  0xc0   gpio	lcdc_data23	trace_data13	uart4exp_sin
gpio0d4 0xff73800c 8  0x300  gpio	lcdc_hsync	trace_data14	pmu_debug2
gpio0d5 0xff73800c 10 0xc00  gpio	lcdc_vsync	trace_data15	pmu_debug3
gpio0d6 0xff73800c 12 0x3000 gpio	lcdc_den	trace_clk	pmu_debug4
gpio0d7 0xff73800c 14 0xc000 gpio	lcdc_dclk	trace_ctl	pmu_debug5
gpio1a0 0xff770000 0  0x3    gpio	cif_data2	ts_data0	0
gpio1a1 0xff770000 2  0xc    gpio	cif_data3	ts_data1	0
gpio1a2 0xff770000 4  0x30   gpio	cif_data4	ts_data2	0
gpio1a3 0xff770000 6  0xc0   gpio	cif_data5	ts_data3	0
gpio1a4 0xff770000 8  0x300  gpio	cif_data6	ts_data4	0
gpio1a5 0xff770000 10 0xc00  gpio	cif_data7	ts_data5	0
gpio1a6 0xff770000 12 0x3000 gpio	cif_data8	ts_data6	0
gpio1a7 0xff770000 14 0xc000 gpio	cif_data9	ts_data7	0
gpio1b0 0xff770004 0  0x3    gpio	cif_vsync	ts_sync	0
gpio1b1 0xff770004 2  0xc    gpio	cif_href	ts_valid	0
gpio1b2 0xff770004 4  0x30   gpio	cif_clkin	ts_clk	0
gpio1b3 0xff770004 6  0xc0   gpio	cif_clkout	ts_fail	0
gpio1b4 0xff770004 8  0x300  gpio	cif_data0	0	0
gpio1b5 0xff770004 10 0xc00  gpio	cif_data1	0	0
gpio1b6 0xff770004 12 0x3000 gpio	cif_data10	spi1_clk	0
gpio1b7 0xff770004 14 0xc000 gpio	cif_data11	spi1_csn0	0
gpio1c0 0xff770008 0  0x3    gpio	i2c3cam_scl	spi1_rxd	0
gpio1c1 0xff770008 2  0xc    gpio	i2c3cam_sda	spi1_txd	0
gpio1c2 0xff770008 4  0x30   gpio	flash_data0	emmc_data0	sfc_sio0
gpio1c3 0xff770008 6  0xc0   gpio	flash_data1	emmc_data1	sfc_sio1
gpio1c4 0xff770008 8  0x300  gpio	flash_data2	emmc_data2	sfc_sio2
gpio1c5 0xff770008 10 0xc00  gpio	flash_data3	emmc_data3	sfc_sio3
gpio1c6 0xff770008 12 0x3000 gpio	flash_data4	emmc_data4	spi0_rxd
gpio1c7 0xff770008 14 0xc000 gpio	flash_data5	emmc_data5	spi0_txd
gpio1d0 0xff77000c 0  0x3    gpio	flash_data6	emmc_data6	spi0_csn0
gpio1d1 0xff77000c 2  0xc    gpio	flash_data7	emmc_data7	spi0_csn1
gpio1d2 0xff77000c 4  0x30   gpio	flash_rdy	emmc_cmd	sfc_clk
gpio1d3 0xff77000c 6  0xc0   gpio	flash_wp	emmc_pwren	0
gpio1d4 0xff77000c 8  0x300  gpio	flash_rdn	sfc_csn1	0
gpio1d5 0xff77000c 10 0xc00  gpio	flash_ale	spi0_clk	0
gpio1d6 0xff77000c 12 0x3000 gpio	flash_cle	0	0
gpio1d7 0xff77000c 14 0xc000 gpio	flash_wrn	sfc_csn0	0
gpio2a0 0xff770010 0  0x3    gpio	flash_csn0	0	0
gpio2a1 0xff770010 2  0xc    gpio	flash_csn1	0	0
gpio2a2 0xff770010 4  0x30   gpio	flash_csn2	0	0
gpio2a3 0xff770010 6  0xc0   gpio	flash_csn3	emmc_rstnout	0
gpio2a4 0xff770010 8  0x300  gpio	flash_dqs	emmc_clkout	0
gpio2a5 0xff770010 10 0xc00  gpio	sdmmc0_data0	uart2dbg_sout	0
gpio2a6 0xff770010 12 0x3000 gpio	sdmmc0_data1	uart2dbg_sin	0
gpio2a7 0xff770010 14 0xc000 gpio	sdmmc0_data2	jtag_tck	0
gpio2b0 0xff770014 0  0x3    gpio	sdmmc0_data3	jtag_tms	0
gpio2b1 0xff770014 2  0xc    gpio	sdmmc0_clkout	mcujtag_tck	0
gpio2b2 0xff770014 4  0x30   gpio	sdmmc0_cmd	mcujtag_tms	0
gpio2b3 0xff770014 6  0xc0   gpio	sdmmc0_dectn	0	0
gpio2b4 0xff770014 8  0x300  gpio	i2s_sclk	0	0
gpio2b5 0xff770014 10 0xc00  gpio	i2s_lrckrx	pcm_sync	0
gpio2b6 0xff770014 12 0x3000 gpio	i2s_lrcktx	0	0
gpio2b7 0xff770014 14 0xc000 gpio	i2s_sdi	0	0
gpio2c0 0xff770018 0  0x3    gpio	i2s_sdo0	0	0
gpio2c1 0xff770018 2  0xc    gpio	i2s_sdo1	pcm_out	0
gpio2c2 0xff770018 4  0x30   gpio	i2s_sdo2	pcm_clk	0
gpio2c3 0xff770018 6  0xc0   gpio	i2s_sdo3	pcm_in	0
gpio2c4 0xff770018 8  0x300  gpio	i2s_clk	0	0
gpio2c5 0xff770018 10 0xc00  gpio	i2c1audio_sda	0	0
gpio2c6 0xff770018 12 0x3000 gpio	i2c1audio_scl	0	0
gpio2c7 0xff770018 14 0xc000 gpio	spdif_tx	edp_hpd	0
gpio2d0 0xff77001c 0  0x3    gpio	uart0bt_sin	0	0
gpio2d1 0xff77001c 2  0xc    gpio	uart0bt_sout	0	0
gpio2d2 0xff77001c 4  0x30   gpio	uart0bt_ctsn	0	0
gpio2d3 0xff77001c 6  0xc0   gpio	uart0bt_rtsn	0	0
gpio2d4 0xff77001c 8  0x300  gpio	sdio0_data0	0	0
gpio2d5 0xff77001c 10 0xc00  gpio	sdio0_data1	0	0
gpio2d6 0xff77001c 12 0x3000 gpio	sdio0_data2	0	0
gpio2d7 0xff77001c 14 0xc000 gpio	sdio0_data3	0	0
gpio3a0 0xff770020 0  0x3    gpio	sdio0_cmd	0	0
gpio3a1 0xff770020 2  0xc    gpio	sdio0_clkout	0	0
gpio3a2 0xff770020 4  0x30   gpio	sdio0_detectn	0	0
gpio3a3 0xff770020 6  0xc0   gpio	sdio0_wrprt	0	0
gpio3a4 0xff770020 8  0x300  gpio	sdio0_pwren	0	0
gpio3a5 0xff770020 10 0xc00  gpio	sdio0_bkpwr	0	0
gpio3a6 0xff770020 12 0x3000 gpio	sdio0_intn	0	0
gpio3a7 0xff770020 14 0xc000 gpio	0	0	0
gpio3b0 0xff770024 0  0x3    gpio	mac_txd0	pwm_0	vop_pwm
gpio3b1 0xff770024 2  0xc    gpio	mac_txd1	0	0
gpio3b2 0xff770024 4  0x30   gpio	mac_txd2	0	0
gpio3b3 0xff770024 6  0xc0   gpio	mac_crs	0	0
gpio3b4 0xff770024 8  0x300  gpio	mac_col	0	0
gpio3b5 0xff770024 10 0xc00  gpio	mac_txen	0	0
gpio3b6 0xff770024 12 0x3000 gpio	mac_txd3	gps_mag	0
gpio3b7 0xff770024 14 0xc000 gpio	mac_rxd0	gps_sig	0
gpio3c0 0xff770028 0  0x3    gpio	mac_rxd1	uart3gps_ctsn	gps_rfclk
gpio3c1 0xff770028 2  0xc    gpio	mac_rxd2	uart3gps_rtsn	usb_drvvbus0
gpio3c2 0xff770028 4  0x30   gpio	mac_rxd3	usb_drvvbus1	0
gpio3c3 0xff770028 6  0xc0   gpio	mac_mdc	isp_shutteren	0
gpio3c4 0xff770028 8  0x300  gpio	mac_rxdv	isp_flashtrigout	0
gpio3c5 0xff770028 10 0xc00  gpio	mac_rxer	isp_prelighttrig	0
gpio3c6 0xff770028 12 0x3000 gpio	mac_clk	isp_shuttertrig	0                                                           
gpio3c7 0xff770028 14 0xc000 gpio	edphdmi_cecinout	isp_flashtrigin	0                                                           
gpio3d0 0xff77002c 0  0x3    gpio	mac_mdio	i2c4tp_sda	0                                                           
gpio3d1 0xff77002c 2  0xc    gpio	mac_rxclkin	i2c4tp_scl	0                                                           
gpio3d2 0xff77002c 4  0x30   gpio	hdmii2c_sda	i2c5hdmi_sda	0                                                           
gpio3d3 0xff77002c 6  0xc0   gpio	hdmii2c_scl	i2c5hdmi_scl	0                                                           
gpio3d4 0xff77002c 8  0x300  gpio	mac_txclkout	spi1_csn1	0                                                           
gpio3d5 0xff77002c 10 0xc00  gpio	ir_rx	uart3gps_sin	0                                                           
gpio3d6 0xff77002c 12 0x3000 gpio	ir_tx	uart3gps_sout	pwm_3                                                           
gpio3d7 0xff77002c 14 0xc000 gpio	sc_vcc18v	i2c2sensor_sda	gpujtag_tck                                                           
);


funcs_table_rk3288=(
gpio0a0 0xff730084 0 0x1 gpio	global_pwroff	0	0	0
gpio0a1 0xff730084 2 0x4 gpio	ddrio_pwroff	0	0	0
gpio0a2 0xff730084 4 0x10 gpio	ddrio0_reten	0	0	0
gpio0a3 0xff730084 5 0xc0 gpio	ddrio1_reten	0	0	0
gpio0a4 0xff730084 -1 rsv gpio	0	0	0	0
gpio0a5 0xff730084 -1 rsv gpio	0	0	0	0
gpio0a6 0xff730084 -1 rsv gpio	0	0	0	0
gpio0a7 0xff730084 -1 rsv gpio	0	0	0	0
gpio0b0 0xff730088 -1 rsv    gpio	0	0	0	0
gpio0b1 0xff730088 -1 rsv    gpio	0	0	0	0
gpio0b2 0xff730088 4 0x10   gpio	tsadc_int	0	0	0
gpio0b3 0xff730088 -1 rsv    gpio	0	0	0	0
gpio0b4 0xff730088 -1 rsv    gpio	0	0	0	0
gpio0b5 0xff730088 10 0x400  gpio	clk_27m	0	0	0
gpio0b6 0xff730088 -1 rsv    gpio	0	0	0	0
gpio0b7 0xff730088 14 0x4000 gpio	i2c0pmu_sda	0	0	0
gpio0c0 0xff73008c 0 0x1    gpio	i2c0pmu_scl	0	0	0
gpio0c1 0xff73008c 2 0xc    gpio	test_clkout	clkt1_27m	0	0
gpio0c2 0xff73008c -1 rsv    gpio	0	0	0	0
gpio1d0 0xff77000c 0 0x1    gpio	lcdc0_hsync	0	0	0
gpio1d1 0xff77000c 2 0x4    gpio	lcdc0_vsync	0	0	0
gpio1d2 0xff77000c 4 0x10   gpio	lcdc0_den	0	0	0
gpio1d3 0xff77000c 6 0x40   gpio	lcdc0_dclk	0	0	0
gpio2a0 0xff770010 0 0x3    gpio	cif_data2	host_din0	hsadc_data0	0
gpio2a1 0xff770010 2 0xc    gpio	cif_data3	host_din1	hsadc_data1	0
gpio2a2 0xff770010 4 0x30   gpio	cif_data4	host_din2	sadc_data2	0
gpio2a3 0xff770010 6 0xc0   gpio	cif_data5	host_din3	hsadc_data3	0
gpio2a4 0xff770010 8 0x300  gpio	cif_data6	host_ckinp	hsadc_data4	0
gpio2a5 0xff770010 10 0xc00  gpio	cif_data7	host_ckinn	hsadc_data5	0
gpio2a6 0xff770010 12 0x3000 gpio	cif_data8	host_din4	hsadc_data6	0
gpio2a7 0xff770010 14 0xc000 gpio	cif_data9	host_din5	hsadc_data7	0
gpio2b0 0xff770014 0 0x3    gpio	cif_vsync	host_din6	hsadcts_sync	0
gpio2b1 0xff770014 2 0xc    gpio	cif_href	host_din7	hsadcts_valid	0
gpio2b2 0xff770014 4 0x30   gpio	cif_clkin	host_wkack	gps_clk	hsadc_clkout
gpio2b3 0xff770014 6 0xc0   gpio	cif_clkout	host_wkreq	hsadcts_fail	0
gpio2b4 0xff770014 8 0x100  gpio	cif_data0	0	0	0
gpio2b5 0xff770014 10 0x400  gpio	cif_data1	0	0	0
gpio2b6 0xff770014 12 0x1000 gpio	cif_data10	0	0	0
gpio2b7 0xff770014 14 0x4000 gpio	cif_data11	0	0	0
gpio2c0 0xff770018 0 0x1   gpio	i2c3cam_scl	0	0	0
gpio2c1 0xff770018 2 0x4   gpio	i2c3cam_sda	0	0	0
gpio3a0 0xff770020 0 0x3    gpio	flash0_data0	emmc_data0	0	0
gpio3a1 0xff770020 2 0xc    gpio	flash0_data1	emmc_data1	0	0
gpio3a2 0xff770020 4 0x30   gpio	flash0_data2	emmc_data2	0	0
gpio3a3 0xff770020 6 0xc0   gpio	flash0_data3	emmc_data3	0	0
gpio3a4 0xff770020 8 0x300  gpio	flash0_data4	emmc_data4	0	0
gpio3a5 0xff770020 10 0xc00  gpio	flash0_data5	emmc_data5	0	0
gpio3a6 0xff770020 12 0x3000 gpio	flash0_data6	emmc_data6	0	0
gpio3a7 0xff770020 14 0xc000 gpio	flash0_data7	emmc_data7	0	0
gpio3b0 0xff770024 0 0x1    gpio	flash0_rdy	0	0	0
gpio3b1 0xff770024 2 0xc    gpio	flash0_wp	emmc_pwren	0	0
gpio3b2 0xff770024 4 0x10   gpio	flash0_rdn	0	0	0
gpio3b3 0xff770024 6 0x40   gpio	flash0_ale	0	0	0
gpio3b4 0xff770024 8 0x100  gpio	flash0_cle	0	0	0
gpio3b5 0xff770024 10 0x400  gpio	flash0_wrn	0	0	0
gpio3b6 0xff770024 12 0x1000 gpio	flash0_csn0	0	0	0
gpio3b7 0xff770024 14 0x4000 gpio	flash0_csn1	0	0	0
gpio3c0 0xff770028 0 0x3    gpio	flash0_csn2	emmc_cmd		0 0
gpio3c1 0xff770028 2 0xc    gpio	flash0_csn3	emmc_rstnout	0	0
gpio3c2 0xff770028 4 0x30   gpio	flash0_dqs	emmc_clkout	0	0
gpio3c3 0xff770028 -1 rsv   gpio	0	0	0	0
gpio3d0 0xff77002c 0 0x7    gpio	flash1_data0	host_dout0	mac_txd2	sdio1_data0
gpio3d1 0xff77002c 4 0x70    gpio	flash1_data1	host_dout1	mac_txd3	sdio1_data1
gpio3d2 0xff77002c 8 0x700   gpio	flash1_data2	host_dout2	mac_rxd2	sdio1_data2
gpio3d3 0xff77002c 12 0x7000   gpio	flash1_data3	host_dout3	mac_rxd3	sdio1_data3
gpio3d4 0xff770030 0 0x7    gpio	flash1_data4	host_dout4	mac_txd0	sdio1_detectn
gpio3d5 0xff770030 4 0x70   gpio	flash1_data5	host_dout5	mac_txd1	sdio1_wrprt
gpio3d6 0xff770030 8 0x700  gpio	flash1_data6	host_dout6	mac_rxd0	sdio1_bkpwr
gpio3d7 0xff770030 12 0x7000 gpio	flash1_data7	host_dout7	mac_rxd1	sdio1_intn
gpio4a0 0xff770034 0 0x7    gpio	flash1_rdy	host_ckoutp	mac_mdc	0
gpio4a1 0xff770034 4 0x70   gpio	flash1_wp	host_ckoutn	mac_rxdv	flash0_csn4
gpio4a2 0xff770034 8 0x700  gpio	flash1_rdn	host_dout8	mac_rxer	flash0_csn5
gpio4a3 0xff770034 12 0x7000 gpio	flash1_ale	host_dout9	mac_clk	flash0_csn6
gpio4a4 0xff770038 0 0x7    gpio	flash1_cle	host_dout10	mac_txen	flash0_csn7
gpio4a5 0xff770038 4 0x70   gpio	flash1_wrn	host_dout11	mac_mdio	0
gpio4a6 0xff770038 8 0x700  gpio	flash1_csn0	host_dout12	mac_rxclk	sdio1_cmd
gpio4a7 0xff770038 12 0x7000 gpio	flash1_csn1	host_dout13	mac_crs	sdio1_clkout
gpio4b0 0xff77003c 0 0x7    gpio	flash1_dqs	host_dout14	mac_col	flash1_csn3
gpio4b1 0xff77003c 4 0x70   gpio	flash1_csn2	host_dout15	mac_txclk	sdio1_pwren
gpio4c0 0xff770044 0 0x1    gpio	uart0bt_sin	0	0	0
gpio4c1 0xff770044 2 0x4    gpio	uart0bt_sout	0	0	0
gpio4c2 0xff770044 4 0x10   gpio	uart0bt_ctsn	0	0	0
gpio4c3 0xff770044 6 0x40   gpio	uart0bt_rtsn	0	0	0
gpio4c4 0xff770044 8 0x100  gpio	sdio0_data0	0	0	0
gpio4c5 0xff770044 10 0x400  gpio	sdio0_data1	0	0	0
gpio4c6 0xff770044 12 0x1000 gpio	sdio0_data2	0	0	0
gpio4c7 0xff770044 14 0x4000 gpio	sdio0_data3	0	0	0
gpio4d0 0xff770048 0 0x1    gpio	sdio0_cmd	0	0	0
gpio4d1 0xff770048 2 0x4    gpio	sdio0_clkout	0	0	0
gpio4d2 0xff770048 4 0x10   gpio	sdio0_detectn	0	0	0
gpio4d3 0xff770048 6 0x40   gpio	sdio0_wrprt	0	0	0
gpio4d4 0xff770048 8 0x100  gpio	sdio0_pwren	0	0	0
gpio4d5 0xff770048 10 0x400  gpio	sdio0_bkpwr	0	0	0
gpio4d6 0xff770048 12 0x1000 gpio	sdio0_intn	0	0	0
gpio4d7 0xff770048 -1 rsv    gpio	0	0	0	0
gpio5b0 0xff770050 0 0x3    gpio	uart1bb_sin	ts0_data0	0	0
gpio5b1 0xff770050 2 0xc    gpio	uart1bb_sout	ts0_data1	0	0
gpio5b2 0xff770050 4 0x30   gpio	uart1bb_ctsn	ts0_data2	0	0
gpio5b3 0xff770050 6 0xc0   gpio	uart1bb_rtsn	ts0_data3	0	0
gpio5b4 0xff770050 8 0x300  gpio	spi0_clk	ts0_data4	uart4exp_ctsn	0
gpio5b5 0xff770050 10 0xc00  gpio	spi0_csn0	ts0_data5	uart4exp_rtsn	0
gpio5b6 0xff770050 12 0x3000 gpio	spi0_txd	ts0_data6	uart4exp_sout	0
gpio5b7 0xff770050 14 0xc000 gpio	spi0_rxd	ts0_data7	uart4exp_sin	0
gpio5c0 0xff770054 0 0x3    gpio	spi0_csn1	ts0_sync	0	0
gpio5c1 0xff770054 2 0x4    gpio	ts0_valid	0	0	0
gpio5c2 0xff770054 4 0x10   gpio	ts0_clk	0	0	0
gpio5c3 0xff770054 6 0x40   gpio	ts0_err	0	0	0
gpio6a0 0xff77005c 0 0x1    gpio	i2s_sclk	0	0	0
gpio6a1 0xff77005c 2 0x4    gpio	i2s_lrckrx	0	0	0
gpio6a2 0xff77005c 4 0x10   gpio	i2s_lrcktx	0	0	0
gpio6a3 0xff77005c 6 0x40   gpio	i2s_sdi	0	0	0
gpio6a4 0xff77005c 8 0x100  gpio	i2s_sdo0	0	0	0
gpio6a5 0xff77005c 10 0x400  gpio	i2s_sdo1	0	0	0
gpio6a6 0xff77005c 12 0x1000 gpio	i2s_sdo2	0	0	0
gpio6a7 0xff77005c 14 0x4000 gpio	i2s_sdo3	0	0	0
gpio6b0 0xff770060 0 0x1    gpio	i2s_clk	0	0	0
gpio6b1 0xff770060 2 0x4    gpio	i2c2audio_sda	0	0	0
gpio6b2 0xff770060 4 0x10   gpio	i2c2audio_scl	0	0	0
gpio6b3 0xff770060 8 0x40   gpio	spdif_tx	0	0	0
gpio6c0 0xff770064 0 0x3    gpio	sdmmc0_data0	jtag_tms	0	0
gpio6c1 0xff770064 2 0xc    gpio	sdmmc0_data1	jtag_trstn	0	0
gpio6c2 0xff770064 4 0x30   gpio	sdmmc0_data2	jtag_tdi	0	0
gpio6c3 0xff770064 6 0xc0   gpio	sdmmc0_data3	jtag_tck	0	0
gpio6c4 0xff770064 8 0x300  gpio	sdmmc0_clkout	jtag_tdo	0	0
gpio6c5 0xff770064 10 0x400  gpio	sdmmc0_cmd	0	0	0
gpio6c6 0xff770064 12 0x1000 gpio	sdmmc0_dectn	0	0	0
gpio7a0 0xff77006c 0 0x3    gpio	pwm_0	vop0_pwm	vop1_pwm	0
gpio7a1 0xff77006c 2 0x4    gpio	pwm_1	0	0	0
gpio7a2 0xff77006c -1 rsv   gpio	0	0	0	0
gpio7a3 0xff77006c -1 rsv   gpio	0	0	0	0
gpio7a4 0xff77006c -1 rsv  gpio	0	0	0	0
gpio7a5 0xff77006c -1 rsv  gpio	0	0	0	0
gpio7a6 0xff77006c -1 rsv gpio	0	0	0	0
gpio7a7 0xff77006c 14 0xc000 gpio	uart3gps_sin	gps_mag	hsadc_data0	0
gpio7b0 0xff770070 0 0x3    gpio	uart3gps_sout	gps_sig	hsadc_data1	0
gpio7b1 0xff770070 2 0xc    gpio	uart3gps_ctsn	gps_rfclk	gps_clk	0
gpio7b2 0xff770070 4 0x30   gpio	uart3gps_rtsn	usb_drvvbus0	0	0
gpio7b3 0xff770070 6 0xc0   gpio	usb_drvvbus1	edp_hotplug	0	0
gpio7b4 0xff770070 8 0x300  gpio	isp_shutteren	spi1_clk	0	0
gpio7b5 0xff770070 10 0xc00  gpio	isp_flashtrigout	spi1_csn0	0	0
gpio7b6 0xff770070 12 0x3000 gpio	isp_prelighttrig	spi1_rxd	0	0
gpio7b7 0xff770070 14 0xc000 gpio	isp_shuttertrig	spi1_txd	0	0
gpio7c0 0xff770074 0 0x3    gpio	isp_flashtrigin	edphdmi_cecinout	0	0
gpio7c1 0xff770074 4 0x10    gpio	i2c4tp_sda	0	0	0
gpio7c2 0xff770074 8 0x100   gpio	i2c4tp_scl	0	0	0
gpio7c3 0xff770074 12 0x3000   gpio	i2c5hdmi_sda	edphdmii2c_sda	0	0
gpio7c4 0xff770078 0 0x3    gpio		i2c5hdmi_scl	edphdmii2c_scl	0	0
gpio7c5 0xff770078 -1 rsv    gpio		0	0	0	0
gpio7c6 0xff770078 8 0x300   gpio		uart2dbg_sin	uart2dbg_sirin	pwm_2	0
gpio7c7 0xff770078 12 0x7000   gpio	uart2dbg_sout	uart2dbg_sirout	pwm_3	edphdmi_cecinout
gpio8a0 0xff770080 0 0x3    gpio	ps2_clk	sc_vcc18v	0	0
gpio8a1 0xff770080 2 0xc    gpio	ps2_data	sc_vcc33v	0	0
gpio8a2 0xff770080 4 0x10   gpio	sc_detect	0	0	0
gpio8a3 0xff770080 6 0xc0   gpio	spi2_csn1	sc_io	0	0
gpio8a4 0xff770080 8 0x300  gpio	i2c1sensor_sda	sc_rst	0	0
gpio8a5 0xff770080 10 0xc00  gpio	i2c1sensor_scl	sc_clk	0	0
gpio8a6 0xff770080 12 0x3000 gpio	spi2_clk	sc_io	0	0
gpio8a7 0xff770080 14 0xc000 gpio	spi2_csn0	sc_detect	0	0
gpio8b0 0xff770084 0 0x3    gpio	spi2_rxd	sc_rst	0	0
gpio8b1 0xff770084 2 0xc    gpio	spi2_txd	sc_clk	0	0
);

#define gpio functions for rk3399
funcs_table_rk3399=(
gpio0a0 0xff320000 0  0x3   gpio testclkout0_clk32k	0	0
gpio0a1 0xff320000 2  0xc   gpio ddrio_pwroff	tcpd_ccdb_en	0
gpio0a2 0xff320000 4  0x30  gpio wifi_26m	0	pmu_debug0
gpio0a3 0xff320000 6  0xc0  gpio sdio0_write_prt	0	pmu_debug1
gpio0a4 0xff320000 8  0x300  gpio sdio0_int_n	0	pmu_debug2
gpio0a5 0xff320000 10 0xc00  gpio emmc_pwren	0	pmu_debug3
gpio0a6 0xff320000 12 0x3000  gpio pwma3_ir	0	pmu_debug4
gpio0a7 0xff320000 14 0xc000  gpio sdmmc0_dectn	0	pmu_debug5
gpio0b0 0xff320004 0  0x3    gpio sdmmc0_wrprt	test_clkout2	0
gpio0b1 0xff320004 2  0xc    gpio pmuio2_1833_volsel	0	0
gpio0b2 0xff320004 4  0x30   gpio 0	0	0  
gpio0b3 0xff320004 6  0xc0   gpio 0	0	0  
gpio0b4 0xff320004 8  0x300  gpio tcpd_vbus_bdis	0	0
gpio0b5 0xff320004 10 0xc00  gpio tcpd_vbus_source3	tcpd_vbus_fdis	0
gpio1a0 0xff320010 0  0x3    gpio isp0_shutter_en	isp1_shutter_en	tcpd_vbus_sink_en
gpio1a1 0xff320010 2  0xc    gpio isp0_shutter_trig	isp1_shutter_trig	tcpd_cc0_vconn_en
gpio1a2 0xff320010 4  0x30   gpio isp0_flashtrigin	isp1_flashtrigin	tcpd_cc1_vconn_en
gpio1a3 0xff320010 6  0xc0   gpio isp0_flashtrigout 	isp1_flashtrigout 	0
gpio1a4 0xff320010 8  0x300  gpio isp0_prelight_trig	isp1_prelight_trig	0
gpio1a5 0xff320010 10 0xc00  gpio ap_pwroff	0	0
gpio1a6 0xff320010 12 0x3000 gpio tsadc_int	0	0
gpio1a7 0xff320010 14 0xc000 gpio pmcu_uart4dbg_rx	spi1_rxd	0
gpio1b0 0xff320014 0  0x3    gpio pmcu_uart4dbg_tx	spi1_txd	0
gpio1b1 0xff320014 2  0xc    gpio pmcu_jtag_tck	spi1_clk	0
gpio1b2 0xff320014 4  0x30   gpio pmcu_jtag_tms	spi1_csn0	0
gpio1b3 0xff320014 6  0xc0   gpio i2c4_sda	0	0
gpio1b4 0xff320014 8  0x300  gpio i2c4_scl	0	0
gpio1b5 0xff320014 10 0xc00  gpio 0	0	0
gpio1b6 0xff320014 12 0x3000 gpio pwmb3_ir	0	0
gpio1b7 0xff320014 14 0xc000 gpio spi3_rxd	i2c0_sda	0
gpio1c0 0xff320018 0  0x3    gpio spi3_txd	i2c0_scl	0
gpio1c1 0xff320018 2  0xc    gpio spi3_clk	0	0
gpio1c2 0xff320018 4  0x30   gpio spi3_csn0	0	0
gpio1c3 0xff320018 6  0xc0   gpio pwm2	0	0
gpio1c4 0xff320018 8  0x300  gpio i2c8_sda	0	0
gpio1c5 0xff320018 10 0xc00  gpio i2c8_scl	0	0
gpio1c6 0xff320018 12 0x3000 gpio testjtag_tdi	tcpd_vbus_source0	0
gpio1c7 0xff320018 14 0xc000 gpio testjtag_tdo	tcpd_vbus_source1	0
gpio1d0 0xff32001c 0 0x3    gpio testjtag_clk	tcpd_vbus_source2	0
gpio2a0 0xff77e000 0  0x3    gpio vop_data[0]	io_cif_data0	i2c2_sda
gpio2a1 0xff77e000 2  0xc    gpio vop_data[1]	io_cif_data1	i2c2_scl
gpio2a2 0xff77e000 4  0x30   gpio vop_data[2]	io_cif_data2	0
gpio2a3 0xff77e000 6  0xc0   gpio vop_data[3]	io_cif_data3	0
gpio2a4 0xff77e000 8  0x300  gpio vop_data[4]	io_cif_data4	0
gpio2a5 0xff77e000 10 0xc00  gpio vop_data[5]	io_cif_data5	0
gpio2a6 0xff77e000 12 0x3000 gpio vop_data[6]	io_cif_data6	0
gpio2a7 0xff77e000 14 0xc000 gpio vop_data[7]	io_cif_data7	i2c7_sda
gpio2b0 0xff77e004 0  0x3    gpio vop_clk	io_cif_vsync	i2c7_scl
gpio2b1 0xff77e004 2  0xc    gpio spi2_rxd	io_cif_href	i2c6_sda
gpio2b2 0xff77e004 4  0x30   gpio spi2_txd	io_cif_clkin	i2c6_scl
gpio2b3 0xff77e004 6  0xc0   gpio spi2_clk	io_cif_clkout	vop_den
gpio2b4 0xff77e004 8  0x300  gpio spi2_csn0	0	0
gpio2c0 0xff77e008 0  0x3    gpio uart0_rx	0	0
gpio2c1 0xff77e008 2  0xc    gpio uart0_tx	0	0
gpio2c2 0xff77e008 4  0x30   gpio uart0_cts_n	0	0
gpio2c3 0xff77e008 6  0xc0   gpio uart0_rts_n	0	0
gpio2c4 0xff77e008 8  0x300  gpio sdio0_data0	spi5_rxd	0
gpio2c5 0xff77e008 10 0xc00  gpio sdio0_data1	spi5_txd	0
gpio2c6 0xff77e008 12 0x3000 gpio sdio0_data2	spi5_clk	0
gpio2c7 0xff77e008 14 0xc000 gpio sdio0_data3	spi5_csn0	0
gpio2d0 0xff77e00c 0  0x3   gpio sdio0_cmd	0	0
gpio2d1 0xff77e00c 2  0xc   gpio sdio0_clkout	test_clkout1	0
gpio2d2 0xff77e00c 4  0x30  gpio sdio0_detect_n	pcie_clkreqn	0
gpio2d3 0xff77e00c 6  0xc0  gpio sdio0_pwr_en	0	0
gpio2d4 0xff77e00c 8  0x300 gpio sdio0_bkpwr	0	0
gpio3a0 0xff77e010 0  0x3    gpio mac_txd2	spi4_rxd	trace_data12
gpio3a1 0xff77e010 2  0xc    gpio mac_txd3	spi4_txd	trace_data13
gpio3a2 0xff77e010 4  0x30   gpio mac_rxd2	spi4_clk	trace_data14
gpio3a3 0xff77e010 6  0xc0   gpio mac_rxd3	spi4_csn0	trace_data15
gpio3a4 0xff77e010 8  0x300  gpio mac_txd0	spi0_rxd	0
gpio3a5 0xff77e010 10 0xc00  gpio mac_txd1	spi0_txd	0
gpio3a6 0xff77e010 12 0x3000 gpio mac_rxd0	spi0_clk	0
gpio3a7 0xff77e010 14 0xc000 gpio mac_rxd1	spi0_csn0	0
gpio3b0 0xff77e014 0  0x3    gpio mac_mdc	spi0_csn1	0
gpio3b1 0xff77e014 2  0xc    gpio mac_rxdv	0	0
gpio3b2 0xff77e014 4  0x30   gpio mac_rxer	i2c5_sda	0
gpio3b3 0xff77e014 6  0xc0   gpio mac_clk	i2c5_scl	0
gpio3b4 0xff77e014 8  0x300  gpio mac_txen	uart1_rx	0
gpio3b5 0xff77e014 10 0xc00  gpio mac_mdio	uart1_tx	0
gpio3b6 0xff77e014 12 0x3000 gpio mac_rxclk	uart3_rx	0
gpio3b7 0xff77e014 14 0xc000 gpio mac_crs	uart3_tx	0
gpio3c0 0xff77e018 0 0x3    gpio mac_col	uart3_cts	0
gpio3c1 0xff77e018 2 0xc    gpio mac_txclk	uart3_rts	0
gpio3d0 0xff77e01c 0  0x3    gpio i2s0_sclk	trace_data0	0
gpio3d1 0xff77e01c 2  0xc    gpio i2s0_lrck_rx	trace_data1	0
gpio3d2 0xff77e01c 4  0x30   gpio i2s0_lrck_tx	trace_data2	0
gpio3d3 0xff77e01c 6  0xc0   gpio i2s0_sdi0	trace_data3	0
gpio3d4 0xff77e01c 8  0x300  gpio i2s0_sdi1sdo3	trace_data4	0
gpio3d5 0xff77e01c 10 0xc00  gpio i2s0_sdi2sdo2	trace_data5	0
gpio3d6 0xff77e01c 12 0x3000 gpio i2s0_sdi3sdo1	trace_data6	0
gpio3d7 0xff77e01c 14 0xc000 gpio i2s0_sdo0	trace_data7	0
gpio4a0 0xff77e020 0  0x3    gpio i2s_clk	trace_ctl	0
gpio4a1 0xff77e020 2  0xc    gpio i2c1_sda	trace_clk	0
gpio4a2 0xff77e020 4  0x30   gpio i2c1_scl	trace_data8	0
gpio4a3 0xff77e020 6  0xc0   gpio i2s1_sclk	trace_data9	0
gpio4a4 0xff77e020 8  0x300  gpio i2s1_lrck_rx	trace_data10	0
gpio4a5 0xff77e020 10 0xc00  gpio i2s1_lrck_tx	trace_data11	0
gpio4a6 0xff77e020 12 0x3000 gpio i2s1_sdi0	0	0
gpio4a7 0xff77e020 14 0xc000 gpio i2s1_sdo0	0	0
gpio4b0 0xff77e024 0  0x3     gpio sdmmc0_data0	uart2dbg_rx	0
gpio4b1 0xff77e024 2  0xc     gpio sdmmc0_data1	uart2dbg_tx	hdcpjtag_trstn
gpio4b2 0xff77e024 4  0x30    gpio sdmmc0_data2	ap_jtag_tck	hdcpjtag_tdi
gpio4b3 0xff77e024 6  0xc0    gpio sdmmc0_data3	ap_jtag_tms	hdcpjtag_tdo
gpio4b4 0xff77e024 8  0x300   gpio sdmmc0_clkout	mcujtag_tck	hdcpjtag_tck
gpio4b5 0xff77e024 10 0xc00   gpio sdmmc0_cmd	mcujtag_tms	hdcpjtag_tms
gpio4c0 0xff77e028 0  0x3    gpio i2c3_sda_hdmi	uart2dbg_rx	hdmii2c_sda
gpio4c1 0xff77e028 2  0xc    gpio i2c3_scl_hdmi	uart2dbg_tx	hdmii2c_scl
gpio4c2 0xff77e028 4  0x30   gpio pwm0	vop0_pwm_cabc	vop1_pwm_cabc
gpio4c3 0xff77e028 6  0xc0   gpio uart2dbg_rx	0	0
gpio4c4 0xff77e028 8  0x300  gpio uart2dbg_tx	0	0
gpio4c5 0xff77e028 10 0xc00  gpio spdif	0	0
gpio4c6 0xff77e028 12 0x3000 gpio pwm1	0	0
gpio4c7 0xff77e028 14 0xc000 gpio hdmi_cecinout	edp_hotplug	0
gpio4d0 0xff77e02c 0  0x3    gpio pcie_clkreqn	0	0
gpio4d1 0xff77e02c 2  0xc    gpio dp_hotplug	0	0
gpio4d2 0xff77e02c 4  0x30   gpio 0	0	0
gpio4d3 0xff77e02c 6  0xc0   gpio 0	0	0
gpio4d4 0xff77e02c 8  0x300  gpio 0	0	0
gpio4d5 0xff77e02c 10 0xc00  gpio 0	0	0
gpio4d6 0xff77e02c 12 0x3000 gpio 0	0	0
);

function show_usage(){
	echo "";
	echo "Usage: $0 [OPTION]...";
	echo "  -r gpioxxx: such as '-r gpio0a0' means read all the functions gpio0a0 support and also tell the current function of gpio0a0";
	echo "  -w gpioxxx -f funcs: such as '-w gpio4d0 -f pcie_clkreqn ' means set the iomux state of gpio4d0 to 'pcie_clkreqn' ";
	echo "  -L : list all the iomux state of gpios";
}

function get_chip_info()
{
	if [ ! -e "/dev/mem" ]; then
		echo "gpio.sh achieved through the 'io' command, please enable the kernel config CONFIG_DEVMEM if you want to use this scripts";
		exit 1;
	fi
	user=$(env | grep USER | cut -d "=" -f 2);
	if [ "$user" != "root" ]; then
		echo "This scripts should be run in root user, please enter 'su' to switch user to root";
		exit 1;
	fi

	chip_type=`getprop ro.board.platform`;
	echo "this chip is " $chip_type;
	if [ "$chip_type" == "rk3399" ]; then
		funcs_table=(`echo ${funcs_table_rk3399[*]}`);
		#define how many functions for one gpio
		funcs_num_per_gpio=4;
	elif [ "$chip_type" == "rk3288" ]; then
		funcs_table=(`echo ${funcs_table_rk3288[*]}`);
		#define how many functions for one gpio
		funcs_num_per_gpio=5;
	elif [ "$chip_type" == "rk3368" ]; then
		funcs_table=(`echo ${funcs_table_rk3368[*]}`);
		#define how many functions for one gpio
		funcs_num_per_gpio=4;
	elif [ "$chip_type" == "rk3326" ]; then
		funcs_table=(`echo ${funcs_table_px30[*]}`);
		#define how many functions for one gpio
		funcs_num_per_gpio=5;
	elif [ "$chip_type" == "px30" ]; then
		funcs_table=(`echo ${funcs_table_px30[*]}`);
		#define how many functions for one gpio
		funcs_num_per_gpio=5;
	else
		echo "$chip_type is not supported yet, please add it for youself";
		exit 1;
	fi
}

# parse the gpio name, the gpio name must be the format gpioxxx, such as "gpio1a3"
# this function which will get 3 variable, use gpio3c1 as an example:
# $gpio_bank will be 3
# $gpio_group will be 2 (which is 'c'-'a')
# $gpio_pin will be 1
# THIS FUNCTION IS NOT USE NOW
function parse_gpio_name()
{
	local in_gpio_name=$1;
	len=`echo ${#in_gpio_name}`;
	if [ $((len)) != 7 ]; then
		echo "Illegal gpio name format";
		show_usage;
		exit;
	fi

	gpio_bank=`expr ${in_gpio_name: 4: 1}`;
	gpio_group_str=`expr ${in_gpio_name: 5: 1}`;
	if [ ${gpio_group_str} == 'a' ]; then
		gpio_group=0;
	elif [ ${gpio_group_str} == 'b' ]; then
		gpio_group=1;
	elif [ ${gpio_group_str} == 'c' ]; then
		gpio_group=2;
	elif [ ${gpio_group_str} == 'd' ]; then
		gpio_group=3;
	else
		gpio_group=-1;
		echo "$in_gpio_name has illegal gpio group $gpio_group_str";
		exit 1;
	fi
	
	gpio_pin=`expr ${in_gpio_name: 6: 1}`;
	if [ $gpio_pin -gt 7 ]; then
		echo "$in_gpio_name is not exist, gpio_pin must less than 8";
		exit 1;
	fi
}


# get iomux state for one gpio
# auguments: 
# $1: the index of gpio name in array $funcs_table
function get_gpio_iomux_state()
{
	local indx=$1;
	local gpio_name=${funcs_table[$indx]};
	local gpio_mask=${funcs_table[$(($indx+3))]};
	local gpio_offset=${funcs_table[$(($indx+2))]};
	local func_str;

	#caculate current function index
	gpio_iomux_val=(`io -4 -l 4 ${funcs_table[$(($indx+1))]} | busybox awk -F':' '{print $2;}'`);
	gpio_iomux_val=`print "0x$gpio_iomux_val"`;
	
	if [[ ${funcs_table[$(($indx+2))]} == "-1" ]] && [[ ${funcs_table[$(($indx+3))]} == "rsv" ]]; then
		#this io only has gpio function
		gpio_func_idx=0;
	else
		gpio_func_idx=$(($((gpio_iomux_val)) & $((gpio_mask))));
		gpio_func_idx=$(($gpio_func_idx >> $((gpio_offset))));
	fi

	if [ $read_gpio_iomux == 1 ]; then
		#list all functions for one gpio, and use "[]" to indicate the current function.
		local i=0;

		printf "%-8s:\t\c" $gpio_name;
		while [ $i -lt $funcs_num_per_gpio ]
		do
			func_str=${funcs_table[$(($1+$i+4))]};
			if [ $gpio_func_idx == $i ]; then
				# For rk3288,if gpio is gpio2b2, there are 5 functions,but the mask is 2 bits, we need to use grf_soc_con5[bit13] to judge the func_3 and func_4:
				# func_0:gpio, according register value: 0b00
				# func_1:cif_clkin,according register value: 0b01
				# func_2:host_wkack,according register value: 0b10
				# func_3:gps_clk,according register value: 0b11, and the grf_soc_con5[bit13]=0
				# func_4:hsadc_clkout,according register value: 0b11,and the grf_soc_con5[bit13]=1
				if [[ "$chip_type" == "rk3288" ]] && [[ "$gpio_name" == "gpio2b2" ]]; then
					if [[ "$func_str" == "gps_clk" ]] || [[ "$func_str" == "hsadc_clkout" ]]; then
						grf_con5_val=(`io -4 -l 4 0xff770258 | busybox awk -F':' '{print $2;}'`);
						grf_con5_val=`print "0x$grf_con5_val"`;
						con5_bit13=$(($((grf_con5_val)) & 0x2000));
						if [ $con5_bit13 == 0 ]; then
							func_str="gps_clk";
						else
							func_str="hsadc_clkout";
						fi
					fi
				fi
				
				printf "[ %-24s ]\t\c" $func_str;
			else
				printf "%-24s\t\c" $func_str;
			fi
			let i+=1;
		done
		print "\n";
	elif [ $list_all_gpio == 1 ]; then
		#only list the current functions for the gpio
		func_str=${funcs_table[$(($1+$gpio_func_idx+4))]};
		if [ $func_str == "0" ]; then
			echo "Something wrong with the parsing of grf reg for $gpio_name,check funcs_table if the function of pin is define as reserved or not";
			exit 1;
		fi
		
		printf "| %-8s: %-20s |\c" $gpio_name $func_str;
	else
		echo "Invalid CALL"
	fi
}

function list_all_iomux_state()
{
	local i=0;
	local j=0;
	funcs_attr_len=`echo ${#funcs_table[*]}`;
	while [ $i -lt $funcs_attr_len ]
	do
		get_gpio_iomux_state $i;
		let i+=$funcs_num_per_gpio+4;
		let j+=1;
		if [ $(($j % 4)) == 0 ]; then
			print "\n";
		fi
	done
}

# Argument
# $1: gpio name
function read_gpio_funcs_and_iomux_stat()
{
	local i=0;
	local gpio_found=0;
	funcs_attr_len=`echo ${#funcs_table[*]}`;
	while [ $i -lt $funcs_attr_len ]
	do
		if [ ${funcs_table[$(($i))]} == "$1" ]; then
			get_gpio_iomux_state $i;
			gpio_found=1;
			# For rk3288,if gpio is gpio2b2, there are 5 functions,but the mask is 2 bits, we need to use grf_soc_con5[bit13] to judge the func_3 and func_4:
			# func_0:gpio, according register value: 0b00
			# func_1:cif_clkin,according register value: 0b01
			# func_2:host_wkack,according register value: 0b10
			# func_3:gps_clk,according register value: 0b11, and the grf_soc_con5[bit13]=0
			# func_4:hsadc_clkout,according register value: 0b11,and the grf_soc_con5[bit13]=1
			if [[ "$chip_type" == "rk3288" ]] && [[ "$1" == "gpio2b2" ]]; then
					echo "If the current iomux function of gpio2b2 is 'gps_clk', you must also use register grf_soc_con5(addr is 0xff770258) to judge"
					echo "if bit13 of grf_soc_con5 is 0 means it's 'gps_clk', otherwise it's 'hsadc_clkout'";
					echo "Please use 'io -4 0xff770258' to check the value of grf_soc_con5";
			fi
			break;
		fi
		let i+=$funcs_num_per_gpio+4;
	done

	if [ $gpio_found == 0 ]; then
			echo "gpio name is Illegal or this gpio is not invalid on this chip.";
			show_usage;
			exit 1;
	fi
}

#set gpio iomux function
# $1 is the gpio name
# $2 is the function string which want to set
function write_gpio_iomux_function()
{
	local i=0;
	local gpio_found=-1;
	local func_found=-1;
	
	if [ $2 == "0" ]; then
		echo "'0' is not a valid function, the string '0' in funcs_table means this function is not defined";
		exit 1;
	fi
	
	funcs_attr_len=`echo ${#funcs_table[*]}`;
	while [ $i -lt $funcs_attr_len ]
	do
		if [ ${funcs_table[$(($i))]} == "$1" ]; then
			gpio_found=$i;
			# gpio is found, then check the function string.
			i=0;
			while [ $i -lt $funcs_num_per_gpio ]
			do
				func_str=${funcs_table[$(($gpio_found+$i+4))]};
				if [ $func_str == $2 ]; then
					func_found=$(($gpio_found+$i+4));
				fi
				let i+=1;
			done
					
			break;
		fi
		let i+=$funcs_num_per_gpio+4;
	done

	if [ $gpio_found == -1 ]; then
		echo "Invalid gpio name $1, gpio name example:'gpio1a1', if the format is correct,please make sure the gpio name is list in array funcs_table";
		show_usage;
		exit 1;
	fi
	if [ $func_found == -1 ]; then
		echo "Invalid function name '$2', please use command 'sh gpio.sh -r $1' to read the avalible function name";
		show_usage;
		exit 1;
	fi

	# For rk3288,if gpio is gpio2b2, there are 5 functions,but the mask is 2 bits, we need to use grf_soc_con5[bit13] to judge the func_3 and func_4:
	# func_0:gpio, according register value: 0b00
	# func_1:cif_clkin,according register value: 0b01
	# func_2:host_wkack,according register value: 0b10
	# func_3:gps_clk,according register value: 0b11, and the grf_soc_con5[bit13]=0
	# func_4:hsadc_clkout,according register value: 0b11,and the grf_soc_con5[bit13]=1
	if [[ "$chip_type" == "rk3288" ]] && [[ "$1" == "gpio2b2" ]]; then
		if [[ "$2" == "gps_clk" ]] || [[ "$2" == "hsadc_clkout" ]]; then
			echo "If you want to set gpio2b2 to 'gps_clk' or 'hsadc_clkout', you must also use register grf_soc_con5(addr is 0xff770258)"
			echo "if bit13 of grf_soc_con5 is 0 means it's 'gps_clk', otherwise it's 'hsadc_clkout'";
			echo "this script only show the result to set iomux to 'gps_clk'"
			echo "please use 'io -4 0xff770258 0x20002000' to config grf_soc_con5 to make gpio2b2 is 'hsadc_clkout'";
			echo "and use 'io -4 0xff770258 0x20000000' to config grf_soc_con5 to make gpio2b2 is 'gps_clk'";
			# Set function to 'gps_clk', because for gpio2b2 the grf register(0xff770014 4 0x30) has only 2bits to indicate the iomux function,
			# so 0b11 indicate the gpio2b2 is 'gps_clk' or 'hsadc_clkout'
			if [ "$2" == "hsadc_clkout" ]; then
				func_found=$func_found-1;
			fi
		fi
	fi

	gpio_iomux_val=(`io -4 -l 4 ${funcs_table[$(($gpio_found+1))]} | busybox awk -F':' '{print $2;}'`);
	gpio_iomux_val=`print "0x$gpio_iomux_val"`;
	print "The original value of iomux_reg [${funcs_table[$(($gpio_found+1))]}] is $gpio_iomux_val";

	if [[ ${funcs_table[$(($gpio_found+2))]} == "-1" ]] && [[ ${funcs_table[$(($gpio_found+3))]} == "rsv" ]]; then
		#this io only has gpio function
		gpio_func_idx=0;
		echo "${funcs_table[$gpio_found]} can only be set to gpio, no need to config";
		exit;
	else
		gpio_mask=${funcs_table[$(($gpio_found+3))]};
		gpio_offset=${funcs_table[$(($gpio_found+2))]};
		gpio_func_idx=$(($((gpio_iomux_val)) & $((gpio_mask))));
		gpio_func_idx=$(($gpio_func_idx >> $((gpio_offset))));
		func_str=${funcs_table[$(($gpio_found+$gpio_func_idx+4))]};
	fi



	if [ $func_str == ${funcs_table[$func_found]} ]; then
		echo "${funcs_table[$gpio_found]} has already been set to function $func_str";
		exit 1;
	fi

	iomux_neg_val=`printf "0x%x" $(($((gpio_mask))^0x0000ffff))`;
	iomux_neg_val=$(($((gpio_iomux_val)) & $(($iomux_neg_val))));

	iomux_set_val=$(($func_found-$gpio_found-4));
	iomux_set_val=$(($iomux_set_val << $((gpio_offset))));
	iomux_set_val=$(($iomux_set_val | $iomux_neg_val));
	wanted_value=$iomux_set_val;
	iomux_set_val=$(($iomux_set_val+$(($((gpio_mask)) << 16))));
	iomux_set_val=`printf "0x%x\n" ${iomux_set_val}`;
	eval $(io -4 ${funcs_table[$(($gpio_found+1))]} $iomux_set_val);

	# read back the grf reg value and judge whether it's equal
	read_value=(`io -4 -l 4 ${funcs_table[$(($gpio_found+1))]} | busybox awk -F':' '{print $2;}'`);
	read_value=`print "0x$read_value"`;
	print "The value of iomux_reg[${funcs_table[$(($gpio_found+1))]}] is $read_value";
	echo "wanted val=$((wanted_value)),read_val=$((read_value))";
	if [ $((read_value)) == $((wanted_value)) ]; then
		echo "Write config successful!"
	else
		echo "Write config failed, please check!"
	fi
}

#initialize in_gpio
in_gpio="";
list_all_gpio=0;
read_gpio_iomux=0;
write_gpio_iomux=0;
write_funcs="";
while getopts "r:w:f:L" arg;
do
	case $arg in
		r)
			in_gpio=$OPTARG;
			read_gpio_iomux=1;
			;;
		w)
			in_gpio=$OPTARG;
			write_gpio_iomux=1;
			;;
		f)
			write_funcs=$OPTARG;
			;;
		L)
			list_all_gpio=1;
			;;
		\?)
			show_usage;
			exit 1;
			;;
	esac
done

if [[ $in_gpio == "" ]] && [[ $list_all_gpio == 0 ]]; then
	show_usage;
	exit 1;
fi

get_chip_info;

if [ $list_all_gpio == 1 ]; then
	list_all_iomux_state;
	exit 1;
fi

if [ $read_gpio_iomux == 1 ]; then
	read_gpio_funcs_and_iomux_stat $in_gpio;
	exit 1;
fi

if [ $write_gpio_iomux == 1 ]; then
	if [ $write_funcs == "" ]; then
		show_usage;
	fi
	write_gpio_iomux_function $in_gpio $write_funcs;
	exit 1;
fi