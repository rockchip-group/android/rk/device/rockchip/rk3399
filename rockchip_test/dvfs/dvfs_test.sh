#!/bin/sh
#############################################
#
# Version Information
#
#   v1.0 first version, only test on rk3399 linux kernel 4.4
#
#		Authors: zwp@rock-chips.com
#
#############################################

echo "**********************stress dvfs test****************************"
function get_chip_info(){
	# the differences between chips are:
	# 1. some chip don't have 2 cpu clusters, this is already done by judge which /sys/devices/system/cpu/cpufreq/policy4/ is exist
	# 2. gpu frequence sysfile is different between chips, this need to be added in the script by judge the ro.product.model
	chip_type=`getprop ro.product.model`
	chip_type=`expr ${chip_type: 0: 6}` # the value will be "rkxxxx"
	echo "this chip is " $chip_type
}

test_times=0 #record test times

function shwo_usage(){
	echo ""
	echo "Usage: $0 [OPTION]..."
	echo "  -c cpu freqence conversion test"
	echo "  -g gpu freqence conversion test"
	echo "  -d ddr freqence conversion test"
	echo "  -n delay n seconds after every operation"
}

# $1 is the array of frequences
# return value is the random freqence
function get_random_freq(){
	local arr
	local array_num
	local random_idx
	
	OLD_IFS="$IFS"
	IFS=" "
	arr=($1)
	IFS="$OLD_IFS"
	array_num=${#arr[@]}
	random_idx=$((RANDOM%($array_num)))
	                                 
	# use echo to set return value
	echo ${arr[$random_idx]}
	return $?
}

function do_cpu_test(){
	cpu_cluster_num=1
	if [ -d "/sys/devices/system/cpu/cpufreq/policy4/" ] ; then
		cpu_cluster_num=2
	fi

	#set ramdon freq for cpu_big
	if [ $cpu_cluster_num -eq 2 ]; then
		echo userspace >  /sys/devices/system/cpu/cpufreq/policy4/scaling_governor
		freq_big=`cat /sys/devices/system/cpu/cpufreq/policy4/scaling_available_frequencies`
	  ret_freq_big=$(get_random_freq "$freq_big")
	  echo "set freq $ret_freq_lit for cpu_big"
	  echo $ret_freq_big > /sys/devices/system/cpu/cpufreq/policy4/scaling_setspeed
	fi

	#set ramdon freq for cpu_little
	echo userspace >  /sys/devices/system/cpu/cpufreq/policy0/scaling_governor
	freq_little=`cat /sys/devices/system/cpu/cpufreq/policy0/scaling_available_frequencies`
	ret_freq_lit=$(get_random_freq "$freq_little")
	echo "set freq $ret_freq_lit for cpu_little"
	echo $ret_freq > /sys/devices/system/cpu/cpufreq/policy0/scaling_setspeed

}

function do_cpu_exit(){
	echo "recover cpu settings"
	
	echo $governor_little_orig > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor
  echo "cur governor of cpuL is:"
  cat /sys/devices/system/cpu/cpufreq/policy0/scaling_governor

	if [ $cpu_cluster_num -eq 2 ]; then
		echo $governor_big_orig > /sys/devices/system/cpu/cpufreq/policy4/scaling_governor
		echo "cur governor of cpub is:"
		cat /sys/devices/system/cpu/cpufreq/policy4/scaling_governor
	fi
}

function do_gpu_test(){
	if [ "$chip_type" == "rk3399" ]; then
		echo userspace >  /sys/class/devfreq/ff9a0000.gpu/governor
		freq_list_gpu=`cat /sys/class/devfreq/ff9a0000.gpu/available_frequencies`
	
		#set ramdon freq to gpu
		ret_freq_gpu=$(get_random_freq "$freq_list_gpu")
		echo "set freq $ret_freq_gpu for gpu"
		echo $ret_freq_gpu > /sys/class/devfreq/ff9a0000.gpu/userspace/set_freq
	else
		echo "not support yet, please add supoort in script for chip $chip_type "
		exit 1
	fi
}

function do_gpu_exit(){
	if [ "$chip_type" == "rk3399" ]; then
		echo "recover gpu settings"
		echo $governor_gpu_orig >  /sys/class/devfreq/ff9a0000.gpu/governor
	  echo "cur governor of gpu is:"
	  cat /sys/class/devfreq/ff9a0000.gpu/governor
 	else
		echo "not support yet, please add supoort in script for chip $chip_type "
		exit 1
	fi
}

function do_ddr_test(){
	if [ -e /sys/class/devfreq/dmc/governor ]; then
		echo userspace > /sys/class/devfreq/dmc/governor
		freq_list_ddr=`cat /sys/class/devfreq/dmc/available_frequencies`
	
		#set ramdon freq to ddr
		ret_freq_ddr=$(get_random_freq "$freq_list_ddr")
		echo "set freq $ret_freq_ddr for ddr"
		echo $ret_freq_ddr > /sys/class/devfreq/dmc/userspace/set_freq
	else
		echo "dmc is not enabled, please enable dmc node in dts!!"
	fi
}

function do_ddr_exit(){
	echo "recover ddr settings"
	echo $governor_ddr_orig > /sys/class/devfreq/dmc/governor
  echo "cur governor of ddr is:"
  cat /sys/class/devfreq/dmc/governor
}

function do_exit(){
	echo "recived SIG ctrl+c or ctrl+z"
	echo "already test times : $test_times"
	if [ $is_cpu_test -eq 1 ] ; then
		do_cpu_exit
	fi
	if [ $is_gpu_test -eq 1 ] ; then
		do_gpu_exit
	fi
	if [ $is_ddr_test -eq 1 ] ; then
		do_ddr_exit
	fi
	exit 1
}

function dvfs_testing(){
	while true
	do
		if [ $is_cpu_test -eq 1 ] ; then
			do_cpu_test
		fi
		if [ $is_gpu_test -eq 1 ] ; then
			do_gpu_test
		fi
		if [ $is_ddr_test -eq 1 ] ; then
			do_ddr_test
		fi
	
		test_times=`expr $test_times + 1`
		sleep $delay_time
	done
}

trap 'do_exit' INT TSTP

is_cpu_test=0
is_gpu_test=0
is_ddr_test=0
delay_time=0

#get chip id, such as "rk3399"
get_chip_info

while getopts "cgdn:" arg;
do
	case $arg in
		c)
			echo "do cpu test"
			if [[ -e /sys/devices/system/cpu/cpufreq/policy0/scaling_governor ]] && [[ -e /sys/devices/system/cpu/cpufreq/policy4/scaling_governor ]]; then
				governor_little_orig=`cat /sys/devices/system/cpu/cpufreq/policy0/scaling_governor`
				governor_big_orig=`cat /sys/devices/system/cpu/cpufreq/policy4/scaling_governor`
				echo "*********original scaling governor of cpu_big: $governor_little_orig, cpu_little: $governor_big_orig !"
				is_cpu_test=1
			fi

			;;
		g)
			echo "do gpu test"
			if [ -e /sys/class/devfreq/ff9a0000.gpu/governor ]; then
				governor_gpu_orig=`cat /sys/class/devfreq/ff9a0000.gpu/governor`
				echo "original governor of gpu is $governor_gpu_orig"
				is_gpu_test=1
			fi

			;;
		d)
			echo "do ddr test"
			if [ -e /sys/class/devfreq/dmc/governor ]; then
				governor_ddr_orig=`cat /sys/class/devfreq/dmc/governor`
				echo "original governor of ddr is $governor_ddr_orig"
				is_ddr_test=1
			fi
			;;
		n)
			echo "delay $OPTARG senconds for every loop"
			delay_time=$OPTARG
			;;
		\?)
			echo "invalid command"
      shwo_usage
      exit 1
      ;;
	esac
done

if [[ $is_cpu_test -eq 0 ]] && [[ $is_gpu_test -eq 0 ]] && [[ $is_ddr_test -eq 0 ]] && [[ $delay_time -eq 0 ]] ; then
	shwo_usage
	exit 0
fi

if [ $delay_time -eq 0 ] ; then
	echo "delay_time is 0, set default value 2"
	let delay_time=2
fi

echo "doing dvfs testing"
dvfs_testing