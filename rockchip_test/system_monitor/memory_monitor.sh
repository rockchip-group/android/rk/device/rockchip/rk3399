#!/system/bin/sh

#############################################
#
#   Authors: cmy@rock-chips.com
#
#############################################

version=0.1

get_ion_info()
{
    if [ -f "/sys/kernel/debug/ion/heaps/cma" ]; then
        echo "CMA:"
        cat /sys/kernel/debug/ion/heaps/cma
    else
        echo "ion's cma heap read failed"
    fi

    if [ -f "/sys/kernel/debug/ion/heaps/vmalloc" ]; then
        echo "Vmalloc:"
        cat /sys/kernel/debug/ion/heaps/vmalloc
    else
        echo "ion's vmalloc heap read failed"
    fi
}

get_drm_info()
{
    if [ -f "/sys/kernel/debug/dri/0/mm_dump" ]; then
        echo "DRM:"
        cat /sys/kernel/debug/dri/0/mm_dump
    else
        echo "drm read failed"
    fi
}

get_gpu_info()
{
# RK3326 + kernel4.4, /sys/kernel/debug/mali0/gpu_memory, unit: KB
# RK3288 + kernel3.10, /sys/kernel/debug/mali0/gpu_memory, unit: KB
    if [ -f "/sys/kernel/debug/mali/gpu_memory" ]; then
        cat /sys/kernel/debug/mali/gpu_memory
    elif [ -f "/sys/kernel/debug/mali0/gpu_memory" ]; then
        cat /sys/kernel/debug/mali0/gpu_memory
    elif [ -f "/sys/kernel/debug/mali0/gpu_usage" ]; then
        cat /sys/kernel/debug/mali0/gpu_usage
    elif [ -f "/sys/kernel/debug/mali/memory_usage" ]; then
        cat /sys/kernel/debug/mali/memory_usage
    elif [ -f "/sys/kernel/debug/pvr/driver_stats" ]; then
        cat /sys/kernel/debug/pvr/driver_stats
    else
        echo "gpu memory read failed"
    fi
}

do_exit()
{
    exit 0
}

show_usage()
{
    echo ""
    echo "Usage: $0 [-i seconds] [-p \"pid list\"]..."
    echo "  -i \t\tinterval(second),default value is 30"
    echo "  -p \t\tpid list"
    echo ""
    echo "example: $0 -i 30 -p \"233 513 679\"\n"
}

#trap 'do_exit' 1 2 3 6 15

echo ""
echo "Memory Monitor Version: "$version
echo "[Model]: "`getprop ro.product.model`
echo "[Firmware]: "`getprop ro.build.description`
echo "[Kernel]: "`cat /proc/version`
echo ""

INTERVAL_S=30 # record every 300 seconds defaultly
PIDs=""

while getopts "i:p:o:" arg
do
    case $arg in
        i)
            INTERVAL_S=$OPTARG
            ;;
        p)
            PIDs=$OPTARG
            ;;
        ?)
            show_usage
            exit 1
            ;;
    esac
done
echo ""

while true
do
    echo "\n========== uptime =========="
    uptime

    echo "\n========== /proc/meminfo =========="
    cat /proc/meminfo
    echo "\n========== sysrq meminfo =========="
    echo 'm' > /proc/sysrq-trigger; dmesg | tail -n 50 | grep -A50 "Show Memory"
    echo "\n========== dumpsys meminfo =========="
    dumpsys meminfo -a
    echo "\n========== procrank =========="
    procrank
    echo "\n========== ps -p -P =========="
    ps -p -P
    echo "\n========== /proc/buddyinfo =========="
    cat /proc/buddyinfo
    echo "\n========== /proc/pagetypeinfo =========="
    cat /proc/pagetypeinfo
    echo "\n========== /proc/vmallocinfo =========="
    cat /proc/vmallocinfo
    echo "\n========== /proc/vmstat =========="
    cat /proc/vmstat
    echo "\n========== /proc/zoneinfo =========="
    cat /proc/zoneinfo

    if [ -d "/sys/kernel/debug/ion" ]; then
        echo "\n========== ION Memory =========="
        get_ion_info
    fi
    
    if [ -d "/sys/kernel/debug/dri" ]; then
        echo "\n========== DRM Memory =========="
        get_drm_info
    fi
    
    if [ -f "/sys/kernel/debug/dma_buf/bufinfo" ]; then
        echo "\n========== DMA Buf =========="
        cat /sys/kernel/debug/dma_buf/bufinfo
    fi

    echo "\n========== GPU Memory =========="
    get_gpu_info
    
    for PID in ${PIDs[@]}
    do
        echo "\n========== showmap $PID =========="
        showmap $PID
    done

    sleep $INTERVAL_S
done

do_exit
